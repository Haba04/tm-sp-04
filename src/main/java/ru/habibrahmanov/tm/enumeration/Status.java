package ru.habibrahmanov.tm.enumeration;

public enum Status {
    PLANNED("planned"),
    INPROGRESS("inprogress"),
    READY("ready");

    private String status;

    public String getStatus() {
        return status;
    }

    Status(String status) {
        this.status = status;
    }

    public String displayName() {
        return status;
    }
}