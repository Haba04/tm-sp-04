package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.enumeration.Status;

import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    void insert(
            @Nullable String name, @Nullable String description, @Nullable String dateBegin,
            @Nullable String dateEnd
    ) throws ParseException;

    Project persist(@Nullable Project project) throws ParseException;

    @Nullable
    Project findOne(@Nullable String projectId);

    @NotNull
    List<Project> findAll();

    @Transactional
    void update(
            @Nullable String projectId, @Nullable String name, @Nullable String description,
            @Nullable Status status, @Nullable String dateBegin, @Nullable String dateEnd
    ) throws ParseException;

    void removeAll();

    void removeOne(@Nullable String projectId);

    @Nullable List<Project> searchByString(@NotNull String userId, @NotNull String string);
}
