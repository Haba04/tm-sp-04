package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;

public abstract class AbstractService {
    @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
}
