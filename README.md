# TASK MANAGER

	TM-SP-04 is a program project manager

# TECHNOLOGY STACK

    Spring MVC 5.0.8
	Maven 4.0
	Java SE 1.8
	Junit 4.11

# DEVELOPER

	Habibrahmanov Ilyas
	habthemes@gmail.com
	I-TEKO

# BUILDING FROM SOURCE

	mvn install

# SOFTWARE REQUIREMENTS

	jdk 1.8

# USING THE PROJECT MANAGER

	From the command-line
	Download the project manager and run it with:
	java -jar D:\Habibrahmanov\tm-sp-04\target\tm-sp-04-1.0-SNAPSHOT.jar